export const environment = {
  production: true,
  languages: ['cs', 'sk', 'de'],
  apiUrl: {
    private: 'http://localhost:7070',
  },
};
