import { NgModule } from '@angular/core';
import { PublicComponent } from './public.component';
import { LoginComponent } from './routes/login/login.component';
import { PublicRoutingModule } from './public-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    PublicComponent,
    LoginComponent
  ],
  imports: [
    SharedModule,
    PublicRoutingModule,
  ]
})
export class PublicModule {
}
