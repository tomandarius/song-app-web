import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './routes/login/login.component';
import { PublicComponent } from './public.component';

export const routes: Routes = [{
  path: '', component: PublicComponent, children: [
    {path: 'login', component: LoginComponent},
    {path: '**', redirectTo: 'login'}
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
