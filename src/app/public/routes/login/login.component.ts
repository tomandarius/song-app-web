import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../security/providers/auth.service';
import { Login } from '../../types';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  hide = true;
  isLoading = false;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    const data: Login = {...this.form.value};
    this.form.disable();
    this.auth.login(data).pipe(
      finalize(() => {
        this.isLoading = false;
        this.form.enable();
      })
    ).subscribe(result => {
      console.log(result);
    });
  }
}
