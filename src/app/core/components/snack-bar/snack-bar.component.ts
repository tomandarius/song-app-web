import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackBarModel, SnackBarTypeEnum } from '../types';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackBarModel,
    private snackbarRef: MatSnackBarRef<SnackBarComponent>
  ) {
  }

  ngOnInit(): void {
  }

  get isInfo(): boolean {
    return this.data.type === SnackBarTypeEnum.info;
  }

  onClose(): void {
    this.snackbarRef.dismiss();
  }
}
