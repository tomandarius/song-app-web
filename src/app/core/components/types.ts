export interface ConfirmDialogModel {
  titleKey: string;
  subtitleKey: string;
  body: string;
}

export interface SnackBarModel {
  titleKey: string;
  body?: string;
  type: SnackBarTypeEnum;
}


export enum SnackBarTypeEnum {
  info = 'info',
  warning = 'warning',
  notifications = 'notifications'
}
