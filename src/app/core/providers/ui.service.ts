import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogModel, SnackBarModel, SnackBarTypeEnum } from '../components/types';
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../components/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
  }


  confirmDialog(titleKey: string, subtitleKey: string, body: string): MatDialogRef<ConfirmDialogComponent> {
    const data: ConfirmDialogModel = {
      titleKey,
      subtitleKey,
      body,
    }

    return this.dialog.open(ConfirmDialogComponent, {
      data,
      width: '350px'
    });
  }

  notify(type: SnackBarTypeEnum, titleKey: string, body?: string): MatSnackBarRef<SnackBarComponent> {
    const data: SnackBarModel = {
      type,
      titleKey,
      body,
    };

    return this.snackBar.openFromComponent(SnackBarComponent, {
      data,
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  }
}
