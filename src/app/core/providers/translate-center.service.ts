import { EventEmitter, Injectable } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslateCenterService {
  public readonly available: string[] = [];

  public get onChange(): EventEmitter<LangChangeEvent> { return this.service.onLangChange; }

  public get used(): string { return this.service.currentLang; }

  constructor(private service: TranslateService) {
    this.available = environment.languages;

    this.service.addLangs(this.available);
    this.service.setDefaultLang(this.available[0]);
  }

  public translate(key: string): any {
    return this.service.instant(key);
  }

  public use(language: string): Observable<any> {
    // this.language.set(language);

    return this.service.use(language);
  }
}
