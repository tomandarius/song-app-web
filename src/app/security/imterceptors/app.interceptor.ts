import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { ActionsService } from '../providers/actions.service';
import { AuthService } from '../providers/auth.service';
import { TokenTypeEnum } from '../types';
import { Token } from '../providers/token';
import { catchError, filter, switchMap, take } from 'rxjs/operators';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshToken$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private actions: ActionsService,
    private auth: AuthService,
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    const isAuth = request.url.includes('/auth/login');


    request = request.clone({headers: request.headers.set('Accept', 'application/json')});

    if (!isAuth) {

      request = this.addToken(request);


      return next.handle(request).pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse && err.status === 401) {
            return this.handle401Error(request, next);
          } else {
            return throwError(err);
          }
        })
      );
    }

    return next.handle(request);
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshToken$.next(null);

      return this.auth.refreshToken().pipe(
        switchMap(response => {
            this.isRefreshing = false;
            this.refreshToken$.next(response.accessToken);
            return next.handle(this.addToken(request));
          },
        ),
        catchError((error) => {
          this.actions.removeTokens();
          this.actions.goToLoginPage();
          return throwError(error);
        }),
      );

    } else {
      return this.refreshToken$.pipe(
        filter(token => token != null),
        take(1),
        switchMap(() => {
          return next.handle(this.addToken(request));
        }));
    }
  }


  private addToken(request: HttpRequest<any>): HttpRequest<any> {
    const token = Token.get(TokenTypeEnum.ACCESS_TOKEN);
    return request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token)});
  }
}
