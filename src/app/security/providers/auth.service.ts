import { Injectable } from '@angular/core';
import { Config } from '../../shared/providers/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginResponse, RefreshTokenModel, TokenTypeEnum } from '../types';
import { tap } from 'rxjs/operators';
import { ActionsService } from './actions.service';
import { Login } from '../../public/types';
import { Token } from './token';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends Config {

  constructor(
    private http: HttpClient,
    private actions: ActionsService
  ) {
    super();
  }

  login(data: Login): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.apiUrl}/auth/login`, data).pipe(
      tap(response => {
        this.actions.saveAccessToken(response.accessToken);
        this.actions.saveRefreshToken(response.refreshToken);
        this.actions.goToPreviewPage();
      }),
    );
  }

  refreshToken() {
    const refreshToken: RefreshTokenModel = {
      refreshToken: Token.get(TokenTypeEnum.REFRESH_TOKEN)
    };
    return this.http.post<LoginResponse>(`${this.apiUrl}/auth/refreshToken`, refreshToken).pipe(
      tap(response => this.actions.saveAccessToken(response.accessToken)),
    );
  }

  logout(): void {
    this.actions.removeTokens();
  }
}
