import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenTypeEnum } from '../types';
import { Token } from './token';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  constructor(
    private router: Router
  ) {

  }

  async goToPreviewPage() {
    await this.router.navigateByUrl('songs-preview');
  }

  async goToLoginPage() {
    await this.router.navigateByUrl('public/login')
  }

  saveAccessToken(token: string): void {
    Token.save(TokenTypeEnum.ACCESS_TOKEN, token);
  }

  saveRefreshToken(token: string): void {
    Token.save(TokenTypeEnum.REFRESH_TOKEN, token);
  }

  removeTokens(): void {
    Token.remove(TokenTypeEnum.REFRESH_TOKEN);
    Token.remove(TokenTypeEnum.ACCESS_TOKEN);
  }
}
