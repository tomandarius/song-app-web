import { DecodedTokenModel, TokenTypeEnum } from '../types';
import * as jwt_decode from 'jwt-decode';

export class Token {

  static save(type: TokenTypeEnum, token: string): void {
    sessionStorage.setItem(type, token);
  }

  static get(type: TokenTypeEnum): string {
    return sessionStorage.getItem(type) as string;
  }

  static remove(type: TokenTypeEnum): void {
    sessionStorage.removeItem(type);
  }

  static get isAccessToken(): boolean {
    return !!this.get(TokenTypeEnum.ACCESS_TOKEN);
  }

  static get decodedAccessToken(): DecodedTokenModel {
    const token = this.get(TokenTypeEnum.ACCESS_TOKEN);
    return jwt_decode.default(token);
  }
}
