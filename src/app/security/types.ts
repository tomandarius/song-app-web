export interface LoginResponse {
  accessToken: string;
  refreshToken: string;
}

export interface RefreshTokenModel {
  refreshToken: string;
}

export interface DecodedTokenModel {
  email: string;
  name: string;
  userId: string;
  iat: number;
  exp: number;
}

export enum TokenTypeEnum {
  ACCESS_TOKEN = 'ACCESS_TOKEN',
  REFRESH_TOKEN = 'REFRESH_TOKEN'
}


