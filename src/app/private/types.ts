export interface Song {
  id?: string;
  title: string;
  text: string;
  number: number;
  note: string;
  audio?: string;
  duration?: number;
  rawChorus?: string;
  file?: File;
  chorus?: string;
}

export interface SongShort {
  id: string;
  title: string;
  text: string;
  number: number;
  note: string;
}

export interface User {
  id: number;
  name: string;
  email: string;
  password?: string;
  type?: UserType;
  access?: AccessType[];
}

export interface Password {
  password: string;
}

// Enums
export enum ActionType {
  SAVE,
  DELETE,
}

export enum UserType {
  ROOT = 'ROOT',
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export enum AccessType {
  CS = 'CS',
  PL = 'PL',
  DE = 'DE',
  SK = 'SK'
}

export enum NetworkState {
  NETWORK_EMPTY,
  NETWORK_IDLE,
  NETWORK_LOADING,
  NETWORK_NO_SOURCE,
}
