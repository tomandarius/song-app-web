import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../security/providers/auth.service';
import { ActionsService } from '../../../security/providers/actions.service';

@Component({
  selector: 'app-menu-toolbar',
  templateUrl: './menu-toolbar.component.html',
  styleUrls: ['./menu-toolbar.component.scss']
})
export class MenuToolbarComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private actions: ActionsService
  ) {
  }

  ngOnInit(): void {
  }

  onLogout(): void {
    this.actions.removeTokens();
    this.actions.goToLoginPage();
  }
}
