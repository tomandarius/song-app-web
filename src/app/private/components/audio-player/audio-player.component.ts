import { Component, Input, EventEmitter, OnChanges, OnInit, Output, OnDestroy } from '@angular/core';
import { NetworkState, Song } from '../../types';
import * as moment from 'moment';
import { MatSliderChange } from '@angular/material/slider';
import { Config } from '../../../shared/providers/config';

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss']
})
export class AudioPlayerComponent implements OnInit, OnChanges, OnDestroy {
  @Input() song: Song;
  config = new Config();
  audio = new Audio();
  isPlaying = false;
  duration = 0;
  current = 0;
  @Output() nexSong = new EventEmitter();
  @Output() previousSong = new EventEmitter();
  @Output() audioState = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {
    this.audio.onplaying = () => {
      this.isPlaying = true;
    }

    this.audio.onloadedmetadata = () => {
      this.duration = Math.floor(this.audio.duration);
    };

    this.audio.ontimeupdate = () => {
      this.current = Math.floor(this.audio.currentTime);

      if (this.current === this.duration) {
        this.current = 0;
        this.audio.currentTime = this.current;
        this.isPlaying = false;
        this.audio.pause();
      }
    };
  }

  ngOnChanges(): void {
    this.isPlaying = false;
    this.audio.pause();
    this.audio.src = `${this.config.apiUrl}/${this.song.id}.mp3`;
    this.audio.title = this.song.title;
    this.audioState.emit(this.isAudioAvailable);
  }

  get isAudioAvailable(): boolean {
    return this.audio.networkState !== NetworkState.NETWORK_NO_SOURCE;
  }

  onPlay(): void {
    if (!this.isPlaying) {
      this.audio.play();
    }
  }

  onPause(): void {
    this.audio.pause();
    this.isPlaying = false;
  }

  onChange(event: MatSliderChange): void {
    this.current = event?.value ?? 0;
    this.audio.currentTime = this.current;
  }

  onNextSong(): void {
    this.nexSong.emit();
  }

  onPreviousSong(): void {
    this.previousSong.emit();
  }

  get currentTime(): string {
    return moment(this.current * 1000).format('mm:ss');
  }

  get remainingTime(): string {
    const result = (this.duration - this.current) * 1000;
    return moment(result).format('mm:ss');
  }

  ngOnDestroy(): void {
    this.audio.pause();
  }
}
