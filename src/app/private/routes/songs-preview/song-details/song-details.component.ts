import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { SongsService } from '../../../providers/songs.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActionType, Song } from '../../../types';
import { filter, finalize, takeUntil } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { UiService } from '../../../../core/providers/ui.service';
import { SnackBarTypeEnum } from '../../../../core/components/types';
import { Converter } from '../../../providers/converter';
import { SongUtils } from '../../../providers/song_utils';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-song-details',
  templateUrl: './song-details.component.html',
  styleUrls: ['./song-details.component.scss']
})
export class SongDetailsComponent implements OnInit, OnDestroy {
  isLoading = false;
  progress = 0;
  song: Song;
  form: FormGroup;
  audio = new Audio();
  private destroy$ = new Subject();

  constructor(
    private songsService: SongsService,
    @Inject(MAT_DIALOG_DATA) public id: string,
    private dialogRef: MatDialogRef<SongDetailsComponent>,
    private fb: FormBuilder,
    private ui: UiService
  ) {
    if (id) {
      this.getData();
    }
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [this.id],
      title: ['', Validators.required],
      text: ['', Validators.required],
      number: [null, Validators.required],
      note: ['', Validators.required],
      audio: [''],
      duration: [''],
      rawChorus: [''],
      chorus: [''],
      file: ['']
    });

    this.convertChorus();
    this.getDurationFromAudio();
  }

  getData(): void {
    this.isLoading = true;
    this.songsService.details(this.id).pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(song => {
      console.log(song);
      this.song = song;
      this.form.patchValue(this.song);
    })
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onDelete(): void {
    this.ui.confirmDialog(
      'global.delete',
      'songDetails.deleteSong',
      this.song.title).afterClosed().subscribe(r => {
      if (r) {
        this.delete();
      }
    })
  }

  onSubmit(): void {

    const file = new FormData();
    file.append('file', this.file);

    const song: Song = {...this.form.value, file};

    console.log(song);

    this.songsService.save(song).subscribe(response => {
      this.onSuccess(ActionType.SAVE, response);
    }, () => this.onError());
  }

  uploadAudio(): void {
    this.progress = 0;
    const file: File = this.form.get('file')?.value;
    console.log(file);

    if (file) {

      this.songsService.uploadAudio(file, this.id).subscribe((event: any) => {

        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        }

      }, () => {
        this.progress = 0;
        this.onError();
      });
    }

  }

  get songTitle(): string {
    return SongUtils.songTitle(this.song);
  }

  get chorus(): FormControl {
    return this.form.get('chorus') as FormControl;
  }

  onFileDropped(evn: Event): void {
    const file = (evn.target as HTMLInputElement).files![0];

    if (file) {
      this.form.get('file')?.setValue(file);
    }
  }

  onRemoveAudio(): void {
    this.songsService.removeAudio(this.id).subscribe(r => console.log(r));
    this.form.get('file')?.reset();
    this.progress = 0;
  }

  get file(): File {
    return this.form.get('file')?.value;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private delete(): void {
    this.songsService.delete(this.id).subscribe(r => {
      this.onSuccess(ActionType.DELETE);
    }, () => this.onError());
  }

  private convertChorus(): void {
    this.form.get('rawChorus')?.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.chorus.patchValue(Converter.convert(result));
      } else {
        this.chorus.patchValue(null);
      }
    });
  }

  private getDurationFromAudio(): void {
    this.form.get('audio')?.valueChanges.pipe(
      takeUntil(this.destroy$),
      filter(r => r !== null)
    ).subscribe(result => {
      this.audio.src = result;

      this.audio.onloadedmetadata = () => {
        this.form.get('duration')?.patchValue(this.audio.duration);
      }

    });
  }

  private onSuccess(type: ActionType, data?: Song): void {
    const isSaved = type === ActionType.SAVE;
    const actionType = isSaved ? 'saved' : 'deleted'
    this.ui.notify(SnackBarTypeEnum.info, 'songDetails.' + actionType);
    this.dialogRef.close(data);
  }

  private onError(): void {
    this.ui.notify(SnackBarTypeEnum.notifications, 'error.unknown');
  }
}
