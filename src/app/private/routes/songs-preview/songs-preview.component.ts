import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SongsService } from '../../providers/songs.service';
import { MatDialog } from '@angular/material/dialog';
import { SongDetailsComponent } from './song-details/song-details.component';
import { debounceTime, filter, finalize, switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Song } from '../../types';
import { SongUtils } from '../../providers/song_utils';

@Component({
  selector: 'app-songs-preview',
  templateUrl: './songs-preview.component.html',
  styleUrls: ['./songs-preview.component.scss']
})
export class SongsPreviewComponent implements OnInit, OnDestroy {
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
  isLoading = false;
  songs: Song[] = [];
  selectedSong: Song;
  destroy$ = new Subject();
  songUtils = SongUtils;
  viewPortHeight: number;
  form: FormGroup;
  audio = new Audio();
  isAudioAvailable = false;

  constructor(
    private songsService: SongsService,
    private dialog: MatDialog,
  ) {
    this.viewPortHeight = window.innerHeight - 210;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      'search': new FormControl('')
    });

    this.fulltextSearch();
    this.getData();
  }

  getData(): void {
    this.isLoading = true;
    this.songsService.songs().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(songs => {
      this.songs = songs;

      if (!this.selectedSong) {
        this.selectedSong = this.songs[0];
        this.audio.src = this.selectedSong?.audio ?? '';
      }
    });
  }


  addSong(): void {
    this.dialog.open(SongDetailsComponent, {
      width: '750px'
    }).afterClosed().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  onSync(): void {
    this.isLoading = true;
    this.songsService.sync().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(songs => {
      console.log(songs);
      this.songs = songs;

      if (!this.selectedSong) {
        this.selectedSong = this.songs[0];
      }

    });
  }

  onSelectSong(song: Song) {
    this.selectedSong = song;
  }

  onEdit(): void {
    this.dialog.open(SongDetailsComponent, {
      data: this.selectedSong.id,
      width: '750px'
    }).afterClosed().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.getData();
        this.selectedSong = result;
      }
    });
  }

  onNextSong(): void {
    const songsLength = this.songWithAudio.length - 1;

    if (this.audioSongIndex < songsLength) {
      this.selectedSong = this.songWithAudio[this.audioSongIndex + 1];
    } else {
      this.selectedSong = this.songWithAudio[0];
    }

    this.scrollTo();
  }

  onAudioState(state: boolean): void {
    this.isAudioAvailable = state;
  }

  onPreviousSong(): void {
    if (this.audioSongIndex !== 0) {
      this.selectedSong = this.songWithAudio[this.audioSongIndex - 1];
    }

    this.scrollTo();
  }

  get isAudio(): boolean {
    return !!this.selectedSong?.audio ?? false;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private fulltextSearch(): void {
    this.form.get('search')?.valueChanges.pipe(
      takeUntil(this.destroy$),
      debounceTime(500),
      switchMap(value => this.songsService.search(value)),
      filter(value => value !== null)
    ).subscribe(songs => {
      this.songs = songs;
      this.selectedSong = this.songs[0];
    });
  }

  private get songWithAudio(): Song[] {
    return this.songs.filter(song => song.audio);
  }

  private get songIndex(): number {
    return this.songs.findIndex(song => song.id === this.selectedSong.id);
  }

  private get audioSongIndex(): number {
    return this.songWithAudio.findIndex(song => song.id === this.selectedSong.id);
  }

  private scrollTo(): void {
    this.viewPort.scrollToIndex(this.songIndex, 'smooth');
  }
}
