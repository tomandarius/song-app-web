import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccessType, ActionType, User, UserType } from '../../../types';
import { UsersService } from '../../../providers/users.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UiService } from '../../../../core/providers/ui.service';
import { MustMatch } from '../../../providers/validators';
import { SnackBarTypeEnum } from '../../../../core/components/types';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  form: FormGroup;
  hide = true;
  accessType = Object.keys(AccessType);
  userType = Object.keys(UserType);

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private dialogRef: MatDialogRef<CreateUserComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User,
    private ui: UiService
  ) {
  }

  ngOnInit(): void {
    const passwordPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}';

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(passwordPattern)]],
      repeatPassword: ['', Validators.required],
      type: ['', Validators.required],
      access: ['', Validators.required],
    }, {
      validators: MustMatch('password', 'repeatPassword')
    });
  }

  onSubmit(): void {
    const user: User = {...this.form.value};

    this.usersService.create(user).subscribe(() => {
      this.onSuccess(ActionType.SAVE);
    }, () => this.onError());

  }

  onClose(): void {
    this.dialogRef.close(false);
  }


  private onSuccess(type: ActionType): void {
    const isSaved = type === ActionType.SAVE;
    const actionType = isSaved ? 'saved' : 'deleted'
    this.ui.notify(SnackBarTypeEnum.info, 'users.' + actionType);
    this.dialogRef.close(true);
  }

  private onError(): void {
    this.ui.notify(SnackBarTypeEnum.notifications, 'error.unknown');
  }
}
