import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AccessType, ActionType, User, UserType } from '../../../types';
import { UsersService } from '../../../providers/users.service';
import { SnackBarTypeEnum } from '../../../../core/components/types';
import { UiService } from '../../../../core/providers/ui.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  form: FormGroup;
  accessType = Object.keys(AccessType);
  userType = Object.keys(UserType);

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private dialogRef: MatDialogRef<UserDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User,
    private ui: UiService
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      type: [''],
      access: [''],
    });

    if (this.user) {
      this.form.patchValue(this.user);
    }
  }

  onSubmit(): void {
    const user: User = {...this.form.value};
    this.usersService.update(user).subscribe(data => {
      this.onSuccess(ActionType.SAVE);
    }, () => this.onError());
  }

  onClose(): void {
    this.dialogRef.close(false);
  }

  onDelete(): void {
    this.ui.confirmDialog(
      'global.delete',
      'users.deleteUser',
      this.user.name).afterClosed().subscribe(r => {
      if (r) {
        this.delete();
      }
    });
  }

  private onSuccess(type: ActionType): void {
    const isSaved = type === ActionType.SAVE;
    const actionType = isSaved ? 'saved' : 'deleted'
    this.ui.notify(SnackBarTypeEnum.info, 'users.' + actionType);
    this.dialogRef.close(true);
  }

  private onError(): void {
    this.ui.notify(SnackBarTypeEnum.notifications, 'error.unknown');
  }

  private delete(): void {
    this.usersService.delete(this.user.id).subscribe(() => {
      this.onSuccess(ActionType.DELETE);
    }, () => this.onError());
  }
}
