import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../../providers/users.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../../types';
import { finalize, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { UserDetailsComponent } from './user-details/user-details.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { Subject } from 'rxjs';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  isLoading = false;
  destroy$ = new Subject();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource: MatTableDataSource<User> = new MatTableDataSource();
  displayedColumns: string[] = ['name', 'email', 'type', 'access', 'edit'];

  constructor(
    private usersService: UsersService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.isLoading = true;
    this.usersService.allUsers().pipe(
      finalize(() => this.isLoading = false)
    ).subscribe(data => {
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  onChangePassword(data: User): void {
    this.dialog.open(ChangePasswordComponent, {
      data: data.id,
      width: '500px'
    })
  }

  onEdit(data: User): void {
    this.dialog.open(UserDetailsComponent, {
      data,
      width: '400px'
    }).afterClosed().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  addUser(): void {
    this.dialog.open(CreateUserComponent, {
      width: '400px'
    }).afterClosed().pipe(
      takeUntil(this.destroy$)
    ).subscribe(result => {
      if (result) {
        this.getData();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
