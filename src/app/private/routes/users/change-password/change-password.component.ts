import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../providers/validators';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UsersService } from '../../../providers/users.service';
import { SnackBarTypeEnum } from '../../../../core/components/types';
import { UiService } from '../../../../core/providers/ui.service';
import { Password } from '../../../types';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  hide = true;
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ChangePasswordComponent>,
    private usersService: UsersService,
    private ui: UiService,
    @Inject(MAT_DIALOG_DATA) public userId: string,
  ) {
  }

  ngOnInit(): void {
    const passwordPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}';

    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.pattern(passwordPattern)]],
      repeatPassword: ['', Validators.required],
    }, {
      validators: MustMatch('password', 'repeatPassword')
    });
  }


  onSubmit(): void {
    const password: Password = {...this.form.value};
    this.usersService.changePassword(this.userId, password).subscribe(() => {
      this.onSuccess();
    }, () => this.onError());
  }

  onClose(): void {
    this.dialogRef.close();
  }


  private onSuccess(): void {
    this.ui.notify(SnackBarTypeEnum.info, 'changePassword.changedPassword');
    this.dialogRef.close();
  }

  private onError(): void {
    this.ui.notify(SnackBarTypeEnum.notifications, 'error.unknown');
  }
}
