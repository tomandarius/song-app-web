import { NgModule } from '@angular/core';
import { PrivateComponent } from './private.component';
import { SharedModule } from '../shared/shared.module';
import { PrivateRoutingModule } from './private-routing.module';
import { SongsPreviewComponent } from './routes/songs-preview/songs-preview.component';
import { SongDetailsComponent } from './routes/songs-preview/song-details/song-details.component';
import { MenuToolbarComponent } from './components/menu-toolbar/menu-toolbar.component';
import { UsersComponent } from './routes/users/users.component';
import { UserDetailsComponent } from './routes/users/user-details/user-details.component';
import { CreateUserComponent } from './routes/users/create-user/create-user.component';
import { ChangePasswordComponent } from './routes/users/change-password/change-password.component';
import { AudioPlayerComponent } from './components/audio-player/audio-player.component';


@NgModule({
  declarations: [
    PrivateComponent,
    SongsPreviewComponent,
    SongDetailsComponent,
    MenuToolbarComponent,
    UsersComponent,
    UserDetailsComponent,
    CreateUserComponent,
    ChangePasswordComponent,
    AudioPlayerComponent
  ],
  imports: [
    SharedModule,
    PrivateRoutingModule,
  ]
})
export class PrivateModule {
}
