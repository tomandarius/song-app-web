import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Song, SongShort } from '../types';
import { Config } from '../../shared/providers/config';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SongsService extends Config {

  constructor(private http: HttpClient) {
    super();
  }

  songs(): Observable<Song[]> {
    const url = `${this.songsUrl}/allComplete`;
    return this.http.get<Song[]>(url).pipe(
      first()
    );
  }

  search(search: string): Observable<Song[]> {
    const prm = new HttpParams().set('search', search);
    const url = `${this.songsUrl}/allComplete`;
    return this.http.get<Song[]>(url, {params: prm}).pipe(
      first()
    );
  }

  sync(): Observable<Song[]> {
    const url = `${this.songsUrl}/sync`
    return this.http.get<Song[]>(url).pipe(
    );
  }

  details(id: string): Observable<Song> {
    const url = `${this.songsUrl}/details/${id}`;
    return this.http.get<Song>(url).pipe(
      first()
    );
  }

  update(song: Song): Observable<Song> {
    const url = `${this.songsUrl}/edit/${song.id}`;
    return this.http.put<Song>(url, song, {reportProgress: true});
  }

  uploadAudio(file: File, songId: string): Observable<any> {
    const url = `${this.songsUrl}/uploadAudio/${songId}`;

    const formData = new FormData();

    formData.append('file', file);

    return this.http.post<any>(url, formData, {reportProgress: true, observe: 'events'});
  }

  removeAudio(songId: string): Observable<any> {
    const url = `${this.songsUrl}/removeAudio/${songId}`;

    return this.http.delete(url);
  }

  create(song: Song): Observable<Song> {
    const url = `${this.songsUrl}/create`;
    return this.http.post<Song>(url, song);
  }

  delete(id: string): Observable<any> {
    const url = `${this.songsUrl}/${id}`;
    return this.http.delete(url);
  }

  save(song: Song): Observable<Song> {
    if (!song.id) {
      return this.create(song);
    }

    return this.update(song);
  }
}
