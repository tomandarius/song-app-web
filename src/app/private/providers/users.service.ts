import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Password, User } from '../types';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../shared/providers/config';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends Config {

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  allUsers(): Observable<User[]> {
    const url = `${this.usersUrl}/allUsers`;
    return this.http.get<User[]>(url).pipe(
      first(),
    );
  }

  details(id: string): Observable<User> {
    const url = `${this.usersUrl}/details/${id}`;
    return this.http.get<User>(url).pipe(
      first()
    )
  }

  create(user: User): Observable<any> {
    const url = `${this.usersUrl}/create`;
    return this.http.post<User>(url, user);
  }

  update(user: User): Observable<any> {
    const url = `${this.usersUrl}/edit/${user.id}`;
    return this.http.put<User>(url, user);
  }

  delete(id: number): Observable<any> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.delete(url);
  }

  changePassword(userId: string, password: Password): Observable<any> {
    const url = `${this.usersUrl}/changePassword/${userId}`;
    return this.http.post(url, password);
  }

  save(user: User): Observable<any> {
    if (!user.id) {
      return this.create(user);
    }

    return this.update(user);
  }
}
