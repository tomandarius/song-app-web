import { Song } from '../types';

export class SongUtils {


  static songTitle(song: Song): string {
    return `${song?.number}. ${song?.title.toUpperCase()} (${song?.note})`;
  }
}
