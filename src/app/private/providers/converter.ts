export class Converter {
  static convert(value: string): string {
    const regexCode = 'AaĄąBbCcĆćDdEeĘęFfGgHhI</>iJjKk?!LlŁłM,.mNnŃńOoÓóPpRrSs-ŚśTtUuWwYyZzŹźŻż';
    const pattern = new RegExp(`(([${regexCode}]*)<sup class="chord">([${regexCode}]*[0-9]*)</sup>([${regexCode}]*))`, 'g');

    return value
      .replace(new RegExp('@', 'g'), '<sup class="chord">')
      .replace(new RegExp('~', 'g'), '</sup>')
      .replace(pattern, '<span class="chord-word">$&</span>')
      .replace(new RegExp('[^\\r\\n]+((\\r|\\n|\\r\\n)[^\\r\\n]+)*', 'g'), '<p>$&</p>')
  }
}
