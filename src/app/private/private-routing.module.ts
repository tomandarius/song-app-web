import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivateComponent } from './private.component';
import { SongsPreviewComponent } from './routes/songs-preview/songs-preview.component';
import { AuthGuard } from '../security/guard/auth.guard';
import { UsersComponent } from './routes/users/users.component';

export const routes: Routes = [{
  path: '', component: PrivateComponent, canActivate: [AuthGuard], children: [
    {path: 'songs-preview', component: SongsPreviewComponent},
    {path: 'users', component: UsersComponent},
    {path: '**', redirectTo: ''}
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule {
}
