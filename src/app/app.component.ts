import { Component } from '@angular/core';
import { TranslateCenterService } from './core/providers/translate-center.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private translator: TranslateCenterService) {
  }
}
