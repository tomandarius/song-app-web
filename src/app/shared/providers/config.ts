import { environment } from '../../../environments/environment';

export class Config {
  apiUrl = environment.apiUrl.private;
  songsUrl = environment.apiUrl.private + '/songs';
  usersUrl = environment.apiUrl.private + '/users';
}
